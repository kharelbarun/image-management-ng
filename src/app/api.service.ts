/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import * as SignIn from "./type/api/SignIn";
import { environment } from '../environments/environment';


@Injectable({
	providedIn: 'root'
})
export class ApiService {

	constructor(
		private http: HttpClient
	) {
	}

	getUrl(path: string): string {
		return environment.apiBaseUrl + path;
	}

	signIn(formData: SignIn.Request): Observable<SignIn.Response> {
		return this.http.post<SignIn.Response>(this.getUrl('/sign_in'), formData);
	}
}
