import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth';

import { ImageEditComponent } from './image-edit/image-edit.component';
import { ImageIndexComponent } from './image-index/image-index.component';
import { ImageNewComponent } from './image-new/image-new.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignOutComponent } from './sign-out/sign-out.component';

const routes: Routes = [
	{ path: '', redirectTo: 'sign-in', pathMatch: 'full' },
	{ path: 'image', component: ImageIndexComponent, canActivate: [ AuthGuard ] },
	{ path: 'image/new', component: ImageNewComponent, canActivate: [ AuthGuard ] },
	{ path: 'image/:id/edit', component: ImageEditComponent, canActivate: [ AuthGuard ] },
	{ path: 'sign-in', component: SignInComponent },
	{ path: 'sign-out', component: SignOutComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
