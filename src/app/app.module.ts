import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignOutComponent } from './sign-out/sign-out.component';
import { ImageIndexComponent } from './image-index/image-index.component';
import { ImageEditComponent } from './image-edit/image-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImageNewComponent } from './image-new/image-new.component';

@NgModule({
	declarations: [
		AppComponent,
		SignInComponent,
		SignOutComponent,
		ImageIndexComponent,
		ImageEditComponent,
		ImageNewComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		ReactiveFormsModule,
		NgbModule,
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
