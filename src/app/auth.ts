/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Injectable } from "@angular/core";
import { CanActivate, Router, UrlTree } from "@angular/router";
import { Observable } from "rxjs";

function getCookie(name: string): string|undefined {
	const value = `; ${document.cookie}`;
	const parts = value.split(`; ${name}=`);
	if (parts.length === 2) {
		return parts.pop()!.split(';').shift();
	}
	return undefined;
}

function deleteCookie(name: string, path?: string, domain?: string) {
	if(getCookie(name)) {
		document.cookie = name + "=" +
		((path) ? ";path="+path:"")+
		((domain)?";domain="+domain:"") +
		";expires=Thu, 01 Jan 1970 00:00:01 GMT";
	}
}

type JwtPayload = {
	iat: number,
	exp: number,
	user: number,
};

function parseJwtPayload(token: string): JwtPayload {
	var base64Url = token.split('.')[1];
	var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
	var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
		return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
	}).join(''));

	return JSON.parse(jsonPayload);
}

const AUTHENTICATION_COOKIE_NAME = 'auth_token';

@Injectable({
	providedIn: 'root',
})
export class AuthService {

	getToken(): string|undefined {
		return getCookie(AUTHENTICATION_COOKIE_NAME);
	}

	isSignedIn(): boolean {
		return Boolean(this.getToken());
	}

	signOut(): void {
		deleteCookie(AUTHENTICATION_COOKIE_NAME);
	}

	signIn(token: string): void {
		const payload = parseJwtPayload(token);
		const expiresAt = new Date(payload.exp * 1000);
		document.cookie = `${AUTHENTICATION_COOKIE_NAME}=${token}; expires=${expiresAt.toUTCString()}`;
	}
}

@Injectable({
	providedIn: 'root',
})
export class AuthGuard implements CanActivate {

	constructor(
		private authService: AuthService,
		private router: Router,
	) {
	}

	canActivate(): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
		if (! this.authService.isSignedIn()) {
			return this.router.parseUrl('sign-in');
		} else {
			return true;
		}
	}
}
