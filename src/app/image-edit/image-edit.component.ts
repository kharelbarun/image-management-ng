/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { concatMap, delay, Observable, of } from 'rxjs';
import { Image } from '../type/Image';
import { ImagesService } from '../images.service';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';

@Component({
	selector: 'app-image-edit',
	templateUrl: './image-edit.component.html',
	styleUrls: ['./image-edit.component.css']
})
export class ImageEditComponent {

	imageId?: string;
	image: Image|null = null;
	imageEditForm: FormGroup;
	isApiRequestInProgress: boolean = false;
	isImageRequestCompleted: boolean = false;
	successMessage: string|null = null;
	failMessage: string|null = null;

	@ViewChild('successAlert', { static: false })
	successAlert?: NgbAlert;

	@ViewChild('failAlert', { static: false })
	failAlert?: NgbAlert;

	constructor(
		private imagesService: ImagesService,
		private route: ActivatedRoute,
	) {
		this.imageEditForm = new FormGroup({
			caption: new FormControl(''),
			altText: new FormControl(''),
			titleText: new FormControl(''),
		});
	}

	ngOnInit() {
		this.imageId = <string> this.route.snapshot.paramMap.get('id');

		this.isApiRequestInProgress = true;
		this.getImage(this.imageId).subscribe({
			next: (image) => {
				this.image = image;
				this.isApiRequestInProgress = false;
				this.isImageRequestCompleted = true;
				this.imageEditForm.setValue({
					caption: this.image.caption,
					altText: this.image.altText,
					titleText: this.image.titleText,
				});
			},
			error: () => {
				this.isApiRequestInProgress = false;
				this.isImageRequestCompleted = true;
			},
		});
	}

	getImage(id: string): Observable<Image> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.imagesService.getImage(id);
			}))
		;
	}

	handleSubmit() {
		if (! this.image) {
			return;
		}
		this.isApiRequestInProgress = true;
		this.updateImage(this.image).subscribe({
			next: () => {
				this.isApiRequestInProgress = false;
				this.successMessage = 'The image was updated.';
				setTimeout(() => {
					this.successAlert?.close();
				}, 5000);
			},
			error: (errorResponse) => {
				this.isApiRequestInProgress = false;
				this.failMessage = 'The image was not updated. Error: ' + errorResponse.error.code;
				setTimeout(() => {
					this.failAlert?.close();
				}, 5000);
			},
		});
	}

	updateImage(image: Image): Observable<any> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.imagesService.updateImage(this.getFormData(image));
			}))
		;
	}

	getFormData(image: Image): Image {
		return {
			id: image.id,
			url: image.url,
			caption: this.imageEditForm.get('caption')?.getRawValue(),
			altText: this.imageEditForm.get('altText')?.getRawValue(),
			titleText: this.imageEditForm.get('titleText')?.getRawValue(),
		};
	}
}
