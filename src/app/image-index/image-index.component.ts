/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { concatMap, delay, Observable, of } from 'rxjs';
import { Image } from '../type/Image';
import { ImagesService } from '../images.service';

@Component({
	selector: 'app-image-list',
	templateUrl: './image-index.component.html',
	styleUrls: ['./image-index.component.css']
})
export class ImageIndexComponent {

	images: Image[] = [];
	isApiRequestInProgress!: boolean;
	isImageRequestCompleted: boolean = false;
	imageToDelete!: Image;
	isDeleteInProgress!: boolean;
	confirmDeleteModal: NgbModalRef|null = null;
	justDeleted: boolean = false;

	constructor(
		private imagesService: ImagesService,
		private modalService: NgbModal,
	) {
	}

	ngOnInit() {
		this.isApiRequestInProgress = true;
		this.getImages().subscribe({
			next: (images) => {
				this.images = images;
				this.isApiRequestInProgress = false;
				this.isImageRequestCompleted = true;
			},
			error: () => {
				this.isApiRequestInProgress = false;
				this.isImageRequestCompleted = true;
			},
		});
	}

	getImages(): Observable<Image[]> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.imagesService.getImages();
			}))
		;
	}

	deleteImage(image: Image): Observable<any> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.imagesService.deleteImage(image);
			}))
		;
	}

	confirmDelete(image: Image, modalContent: TemplateRef<any>) {
		this.imageToDelete = image;
		this.confirmDeleteModal = this.modalService.open(modalContent);
		this.confirmDeleteModal.result.then(
			(result) => {
			},
			(dismissReason) => {
			},
		).finally(() => {
			this.confirmDeleteModal = null;
		});
	}

	handleDeleteClick(image: Image) {
		this.isDeleteInProgress = true;
		const postDeleteSuccess = () => {
			this.isDeleteInProgress = false;
			this.confirmDeleteModal?.close();
			this.justDeleted = true;
			setTimeout(() => {
				const index = this.images.indexOf(image);
				if (index !== -1) {
					this.images.splice(index, 1);
					this.justDeleted = false;
				}
			}, 800);
		};
		
		this.deleteImage(image).subscribe({
			next: () => {
				this.isDeleteInProgress = false;
				postDeleteSuccess();
			},
			error: () => {
				this.isDeleteInProgress = false;
			},
		});
	}
}
