/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { concatMap, delay, Observable, of } from 'rxjs';
import { NewImage } from '../type/NewImage';
import { ImagesService } from '../images.service';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-image-new',
	templateUrl: './image-new.component.html',
	styleUrls: ['./image-new.component.css']
})
export class ImageNewComponent {

	previewUrl: string|null = null;
	imageNewForm: FormGroup;

	isSaveInProgress!: boolean;
	successMessage: string|null = null;
	failMessage: string|null = null;

	@ViewChild('successAlert', { static: false })
	successAlert?: NgbAlert;

	@ViewChild('failAlert', { static: false })
	failAlert?: NgbAlert;

	constructor(
		private imagesService: ImagesService,
		private sanitizer: DomSanitizer,
	) {
		this.imageNewForm = new FormGroup({
			file: new FormControl(''),
			caption: new FormControl(''),
			altText: new FormControl(''),
			titleText: new FormControl(''),
		});
	}

	ngOnInit() {
	}

	handleFileSelect(event: Event) {
		const files = (event.target as HTMLInputElement).files!;
		if (! files.length) {
			return;
		}

		const file = files[0];

		this.previewUrl = <string>this.sanitizer.bypassSecurityTrustResourceUrl(URL.createObjectURL(file));

		this.imageNewForm.controls['file'].patchValue(file);
		this.imageNewForm.get('file')!.updateValueAndValidity();
	}

	handleSubmit() {
		this.isSaveInProgress = true;
		const formData = new FormData();
		Object.keys(this.imageNewForm.controls).forEach(formControlName => {
			formData.append(
				formControlName,
				this.imageNewForm.get(formControlName)!.value
			)
		});
		this.createImage(formData)
			.subscribe({
				next: () => {
					this.isSaveInProgress = false;
					this.successMessage = 'New image was saved.';
					setTimeout(() => {
						this.successAlert?.close();
					}, 5000);

					this.previewUrl = null;
					this.imageNewForm.reset();
				},
				error: (errorResponse: HttpErrorResponse) => {
					this.isSaveInProgress = false;
					this.failMessage = `New image was not saved. Error: ${errorResponse.error.code} ${errorResponse.error.message}`;
					setTimeout(() => {
						this.failAlert?.close();
					}, 5000);
				},
			})
		;
	}

	createImage(formData: FormData): Observable<any> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.imagesService.createImage(formData);
			}))
		;
	}

	getFormData(): NewImage {
		return {
			previewUrl: '',
			caption: this.imageNewForm.get('caption')?.getRawValue(),
			altText: this.imageNewForm.get('altText')?.getRawValue(),
			titleText: this.imageNewForm.get('titleText')?.getRawValue(),
		};
	}
}
