/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { Image } from './type/Image';
import { environment } from '../environments/environment';
import * as ImageNewApi from './type/api/ImageNew';
import * as ImageUpdateApi from './type/api/ImageUpdate';
import { AuthService } from './auth';

@Injectable({
	providedIn: 'root'
})
export class ImagesService {

	constructor(
		private authService: AuthService,
		private http: HttpClient,
	) {
	}

	getUrl(path: string): string {
		return environment.apiBaseUrl + path;
	}

	getTokenOrEmptyString() {
		return this.authService.getToken() ?? '';
	}

	getImages(): Observable<Image[]> {
		return this.http.get<any[]>(
			this.getUrl('/image/'),
			{
				headers: {
					Authorization: `Bearer ${this.getTokenOrEmptyString()}`,
				},
			},
		)
			.pipe(
				map(images => images.map(image => ({
					id: image.id,
					url: image.url,
					caption: image.caption,
					altText: image.alt_text,
					titleText: image.title_text,
				})))
			)
		;
	}

	getImage(id: string): Observable<Image> {
		return this.http.get<any>(
			this.getUrl('/image/'+id),
			{
				headers: {
					Authorization: `Bearer ${this.getTokenOrEmptyString()}`,
				},
			},
		)
			.pipe(
				map(image => ({
					id: image.id,
					url: image.url,
					caption: image.caption,
					altText: image.alt_text,
					titleText: image.title_text,
				}))
			)
		;
	}

	createImage(formData: FormData): Observable<any> {
		return this.http.post<ImageNewApi.Response>(
			this.getUrl('/image/'),
			formData,
			{
				headers: {
					Authorization: `Bearer ${this.getTokenOrEmptyString()}`,
				},
			},
		);
	}

	updateImage(image: Image): Observable<any> {
		const requestData: ImageUpdateApi.Request = {
			caption: image.caption,
			alt_text: image.altText,
			title_text: image.titleText,
		};
		return this.http.put<ImageUpdateApi.Response>(
			this.getUrl('/image/'+image.id),
			requestData,
			{
				headers: {
					Authorization: `Bearer ${this.getTokenOrEmptyString()}`,
				},
			},
		);
	}

	deleteImage(image: Image): Observable<any> {
		return this.http.delete(
			this.getUrl('/image/'+image.id),
			{
				headers: {
					Authorization: `Bearer ${this.getTokenOrEmptyString()}`,
				},
			},
		);
	}
}
