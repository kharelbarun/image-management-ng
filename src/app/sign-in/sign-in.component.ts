/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { concatMap, delay, Observable, of } from 'rxjs';
import { ApiService } from '../api.service';
import { AuthService } from '../auth';
import Error from '../type/api/Error';
import * as SignIn from "../type/api/SignIn";

@Component({
	selector: 'app-sign-in',
	templateUrl: './sign-in.component.html',
	styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {

	signInForm: FormGroup;
	apiRequestInProgress: boolean = false;
	formInvalid: boolean = false;
	wasValidated: boolean = false;
	errorMessage: string|null = null;

	constructor(
		private apiService: ApiService,
		private authService: AuthService,
		private router: Router,
	) {
		this.signInForm = new FormGroup({
			username: new FormControl(''),
			password: new FormControl(''),
		});
	}

	ngOnInit() {
		if (this.authService.isSignedIn()) {
			this.router.navigate(['image/']);
		}
	}

	handleSubmit() {

		this.formInvalid = this.signInForm.invalid;
		this.wasValidated = true;

		if (this.formInvalid) {
			return;
		}

		this.apiRequestInProgress = true;
		this.signIn(this.signInForm.value)
			.subscribe({
				next: (response) => {
					this.authService.signIn(response.auth_token);
					this.apiRequestInProgress = false;
					this.errorMessage = null;
					this.router.navigate(['image/']);
				},
				error: (response: HttpErrorResponse) => {
					this.apiRequestInProgress = false;
					this.errorMessage = (<Error>response.error).message;
				},
			})
		;
	}

	signIn(formData: SignIn.Request): Observable<SignIn.Response> {
		return of(undefined)
			.pipe(delay(1000))
			.pipe(concatMap(() => {
				return this.apiService.signIn(formData);
			}))
		;
	}
}
