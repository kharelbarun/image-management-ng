/*
 * (c) Barun Kharel <kharelbarun@gmail.com>
 */

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth';

@Component({
	selector: 'app-sign-in',
	template: '',
})
export class SignOutComponent {

	constructor(
		private authService: AuthService,
		private router: Router,
	) {
	}

	ngOnInit() {
		this.authService.signOut();
		this.router.navigate(['sign-in']);
	}
}
