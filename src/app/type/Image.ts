export interface Image {
	id: string,
	url: string,
	caption: string,
	altText: string,
	titleText: string,
}
