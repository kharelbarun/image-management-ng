export interface NewImage {
	previewUrl: string|null,
	caption: string,
	altText: string,
	titleText: string,
}
