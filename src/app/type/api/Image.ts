export interface Response {
	caption: string,
	alt_text: string,
	title_text: string,
}
