import * as Image from "./Image";

export type Response = Image.Response[];
