export interface Request {
	caption: string,
	alt_text: string,
	title_text: string,
}

export interface Response {
}
