export interface Request {
	username: string,
	password: string,
}

export interface Response {
	code: string,
	auth_token: string,
}
